# 微信小程序模块初始化

```php
use Tcnet\WorkWechat\Factory;

$config = [
    'app_id' => 'xxx',
    'secret' => 'xxx',
];

$app = Factory::miniProgram($config);
```
