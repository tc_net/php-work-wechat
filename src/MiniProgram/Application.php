<?php

namespace Tcnet\WorkWechat\MiniProgram;

use Tcnet\WorkWechat\Kernel\ServiceContainer;

class Application extends ServiceContainer
{
    protected $providers = [
        Auth\ServiceProvider::class,
        AppCode\ServiceProvider::class,
        PhoneNumber\ServiceProvider::class,
    ];
}