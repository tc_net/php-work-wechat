<?php
namespace Tcnet\WorkWechat\OffiaAccount;

use Tcnet\WorkWechat\Kernel\ServiceContainer;

class Application extends ServiceContainer
{
    protected $providers = [
        Auth\ServiceProvider::class,
        Jssdk\ServiceProvider::class,
    ];
}
