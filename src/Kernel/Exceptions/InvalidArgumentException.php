<?php
/**
 * FILE: InvalidArgumentException.php.
 * User: zhoulei
 * Date: 2022/4/29 15:49
 */
namespace Tcnet\WorkWechat\Kernel\Exceptions;

class InvalidArgumentException extends Exception
{
}